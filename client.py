from flask import Flask, jsonify, request
import requests
from appJar import gui
from time import time
import logging

flask = Flask(__name__)

class Networking:
    def __init__(self):
        logging.getLogger('werkzeug').disabled = True

        aj.queueFunction(aj.setLabel, 'r_message', 'Listening...')
        flask.run(port=54545)

    def send_message(self):
        data = {
            'message': aj.entry('message')
        }
        requests.post('http://{ip}:{port}/msg'.format(ip=ip, port=port), json=data)
    
    @flask.route('/msg', methods=['POST'])
    def message_recieved(self=None, time_recieved=time()):
        data = request.json
        if data:
            aj.queueFunction(aj.setLabel, 'r_message', data['message'])
        return jsonify({
            'r': time_recieved
        })

with gui('Login Window', '400x200', bg='orange', font={'size':18}) as aj:
    aj.entry('message', label=True)
    aj.label('r_message', 'Starting listener')
    ip = aj.stringBox('ip', 'Peer IP address', parent=None)
    port = aj.stringBox('port', 'Connection port', parent=None)
    aj.buttons(['Submit', 'Cancel'], [Networking.send_message, aj.stop])
    aj.thread(Networking)
