import requests
import sys

if len(sys.argv) >= 3:
    url = 'http://127.0.0.1:' + str(sys.argv[1] + '/msg')
    data = {
        'message': str(sys.argv[2])
    }
    requests.post(url, json=data)
    
else:
    message = ''

    peer = input('Peer (127.0.0.1:54545): ')
    if peer == '':
        peer = '127.0.0.1:54545'

    url = 'http://' + peer + '/msg'

    while message != 'bye':
        message = input('Message: ')
        data = {
            'message': message
        }
        requests.post(url, json=data)
        print('Sent!')
